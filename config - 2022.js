var config = {
  rumah_konfirmasi: true,
  sipd: {
    id_daerah: 75,
    nama_daerah: "Kab. Sleman",
    perda: {
      nomor: "7 Tahun 2021",
      tanggal: "13-12-2020",
    },
    penjabaran: {
      nomor: "53.1 Tahun 2021",
      tanggal: "13-12-2020",
      divide_nilai: false,
    },
    dasar_hukum: {
      perda: false, // tmp
      penjabaran: false, // tmp
      gaji: "Peraturan Pemerintah Nomor 15 Tahun 2019 tentang Perubahan Kedelapan Belas atas Peraturan Pemerintah Nomor 7 Tahun 1977 tentang Peraturan Gaji PNS",
      pendapatan: [
        {
          akun: "4.1.01.06.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 9 Tahun 2015 tentang Perubahan atas Peraturan Daerah Nomor 1 Tahun 2011 tentang Pajak Hotel",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.06.07",
          dasar_hukum:
            "Peraturan Daerah Nomor 9 Tahun 2015 tentang Perubahan atas Peraturan Daerah Nomor 1 Tahun 2011 tentang Pajak Hotel",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.06.08",
          dasar_hukum:
            "Peraturan Daerah Nomor 9 Tahun 2015 tentang Perubahan atas Peraturan Daerah Nomor 1 Tahun 2011 tentang Pajak Hotel",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.07.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 10 Tahun 2015 tentang Perubahan atas Peraturan Daerah Nomor 2 Tahun 2011 tentang Pajak Restoran",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.07.03",
          dasar_hukum:
            "Peraturan Daerah Nomor 10 Tahun 2015 tentang Perubahan atas Peraturan Daerah Nomor 2 Tahun 2011 tentang Pajak Restoran",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.07.07",
          dasar_hukum:
            "Peraturan Daerah Nomor 10 Tahun 2015 tentang Perubahan atas Peraturan Daerah Nomor 2 Tahun 2011 tentang Pajak Restoran",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.08.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 13 Tahun 2013 tentang Perubahan atas Peraturan Daerah Nomor 3 Tahun 2011 tentang Pajak Hiburan",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.08.02",
          dasar_hukum:
            "Peraturan Daerah Nomor 13 Tahun 2013 tentang Perubahan atas Peraturan Daerah Nomor 3 Tahun 2011 tentang Pajak Hiburan",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.08.04",
          dasar_hukum:
            "Peraturan Daerah Nomor 13 Tahun 2013 tentang Perubahan atas Peraturan Daerah Nomor 3 Tahun 2011 tentang Pajak Hiburan",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.08.05",
          dasar_hukum:
            "Peraturan Daerah Nomor 13 Tahun 2013 tentang Perubahan atas Peraturan Daerah Nomor 3 Tahun 2011 tentang Pajak Hiburan",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.08.08",
          dasar_hukum:
            "Peraturan Daerah Nomor 13 Tahun 2013 tentang Perubahan atas Peraturan Daerah Nomor 3 Tahun 2011 tentang Pajak Hiburan",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.08.09",
          dasar_hukum:
            "Peraturan Daerah Nomor 13 Tahun 2013 tentang Perubahan atas Peraturan Daerah Nomor 3 Tahun 2011 tentang Pajak Hiburan",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.08.10",
          dasar_hukum:
            "Peraturan Daerah Nomor 13 Tahun 2013 tentang Perubahan atas Peraturan Daerah Nomor 3 Tahun 2011 tentang Pajak Hiburan",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.09.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 4 Tahun 2011 tentang Pajak Reklame",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.09.02",
          dasar_hukum:
            "Peraturan Daerah Nomor 4 Tahun 2011 tentang Pajak Reklame",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.09.05",
          dasar_hukum:
            "Peraturan Daerah Nomor 4 Tahun 2011 tentang Pajak Reklame",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.10.02",
          dasar_hukum:
            "Peraturan Daerah Nomor 9 Tahun 2011 tentang Pajak Penerangan Jalan",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.11.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 11 Tahun 2011 tentang Pajak Parkir",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.12.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 13 Tahun 2010 Tentang Pajak Air Tanah",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.14.12",
          dasar_hukum:
            "Peraturan Daerah Nomor 10 Tahun 2011 tentang Pajak Mineral Bukan Logam",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.14.23",
          dasar_hukum:
            "Peraturan Daerah Nomor 10 Tahun 2011 tentang Pajak Mineral Bukan Logam",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.14.37",
          dasar_hukum:
            "Peraturan Daerah Nomor 10 Tahun 2011 tentang Pajak Mineral Bukan Logam",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.15.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 11 Tahun 2012 tentang Pajak Bumi dan Bangunan Perdesaan dan Perkotaan",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.16.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 14 Tahun 2010 tentang Bea Perolehan Atas Tanah dan Bangunan",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.01.16.02",
          dasar_hukum:
            "Peraturan Daerah Nomor 14 Tahun 2010 tentang Bea Perolehan Atas Tanah dan Bangunan",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.02.01.02",
          dasar_hukum:
            "Peraturan Daerah Nomor 13 Tahun 2011 tentang Retribusi Pelayanan Persampahan/ Kebersihan",
          skpd: ["2.11.1.03.0.00.01.0000", "3.30.3.31.0.00.01.0000"],
        },
        {
          akun: "4.1.02.01.03",
          dasar_hukum:
            "Peraturan Daerah No. 15 Tahun 2011 tentang Retribusi Pelayanan Pemakaman dan Pengabuan Mayat",
          skpd: ["1.03.1.04.0.00.01.0000"],
        },
        {
          akun: "4.1.02.01.04",
          dasar_hukum:
            "Peraturan Daerah Nomor 1 Tahun 2012 tentang Retribusi Pelayanan Parkir di Tepi Jalan Umum",
          skpd: ["2.15.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.02.01.05",
          dasar_hukum:
            "Peraturan Daerah Nomor 2 Tahun 2012 tentang Retribusi Pelayanan Pasar",
          skpd: ["3.30.3.31.0.00.01.0000", "3.27.2.09.3.25.01.0000"],
        },
        {
          akun: "4.1.02.01.06",
          dasar_hukum:
            "Peraturan Daerah Nomor 3 Tahun 2012 tentang Retribusi Pengujian Kendaraan Bermotor",
          skpd: ["2.15.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.02.01.09",
          dasar_hukum:
            "Peraturan Daerah Nomor 4 Tahun 2018 tentang Retribusi Penyediaan dan/atau Penyedotan Kakus",
          skpd: ["2.11.1.03.0.00.01.0000"],
        },
        {
          akun: "4.1.02.01.10",
          dasar_hukum:
            "Peraturan Daerah Nomor 9 Tahun 2018 tentang Perubahan Atas Peraturan Daerah Nomor 13 Tahun 2011 Tentang Retribusi Pelayanan Persampahan/Kebersihan",
          skpd: ["2.11.1.03.0.00.01.0000"],
        },
        {
          akun: "4.1.02.01.11",
          dasar_hukum:
            "Peraturan Daerah Nomor 3 Tahun 2018 tentang Retribusi Pelayanan Tera/Tera Ulang",
          skpd: ["3.30.3.31.0.00.01.0000"],
        },
        {
          akun: "4.1.02.01.12",
          dasar_hukum:
            "Peraturan Daerah Nomor 5 Tahun 2013 tentang Retribusi Pelayanan Pendidikan",
          skpd: ["2.07.3.32.0.00.01.0000"],
        },
        {
          akun: "4.1.02.01.13",
          dasar_hukum:
            "Peraturan Daerah Nomor 12 Tahun 2016 tentang Perubahan Atas Peraturan Daerah Nomor 4 Tahun 2012 tentang Retribusi Pengendalian Menara Telekomunikasi",
          skpd: ["2.16.2.20.2.21.01.0000"],
        },
        {
          akun: "4.1.02.02.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 4 Tahun 2016 tentang Perubahan atas Peraturan Daerah Nomor 5 Tahun 2012 tentang Retribusi Pemakaian Kekayaan Daerah",
          skpd: [
            "1.03.1.04.0.00.01.0000",
            "2.22.0.00.0.00.01.0000",
            "3.26.0.00.0.00.01.0000",
            "3.27.2.09.3.25.01.0000",
            "3.30.3.31.0.00.01.0000",
            "5.02.0.00.0.00.01.0000",
          ],
        },
        {
          akun: "4.1.02.02.04",
          dasar_hukum:
            "Peraturan Daerah Nomor 6 Tahun 2012 tentang Retribusi Terminal",
          skpd: ["2.15.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.02.02.05",
          dasar_hukum:
            "Peraturan Daerah Nomor 15 Tahun 2012 tentang Retribusi Tempat Parkir Khusus",
          skpd: ["2.15.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.02.02.07",
          dasar_hukum:
            "Peraturan Daerah Nomor 7 Tahun 2012 tentang Retribusi Rumah Potong Hewan LD Kab.Sleman Tahun 2010 No.7 Seri C",
          skpd: ["3.27.2.09.3.25.01.0000"],
        },
        {
          akun: "4.1.02.02.09",
          dasar_hukum:
            "Peraturan Daerah Nomor 3 Tahun 2016 tentang Perubahan atas Peraturan Daerah Nomor 12 Tahun 2010 tentang Retribusi Tempat Rekreasi dan Olahraga",
          skpd: [
            "2.22.0.00.0.00.01.0000",
            "3.26.0.00.0.00.01.0000",
            "5.02.0.00.0.00.01.0000",
          ],
        },
        {
          akun: "4.1.02.02.11",
          dasar_hukum:
            "Peraturan Daerah Nomor 10 Tahun 2012 tentang Retribusi Penjualan Produk Usaha Daerah LD Kab.Sleman Tahun 2012 No 10 Seri C",
          skpd: ["3.27.2.09.3.25.01.0000"],
        },
        {
          akun: "4.1.02.03.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 6 Tahun 2011 tentang Retribusi Izin Mendirikan Bangunan",
          skpd: ["2.18.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.02.03.06",
          dasar_hukum:
            "Peraturan Daerah Nomor 16 Tahun 2013 tentang Retribusi Perpanjang Izin Memperkerjakan Tenaga Kerja Asing",
          skpd: ["2.07.3.32.0.00.01.0000"],
        },
        {
          akun: "4.1.03.02.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 6 Tahun 2016 tentang Perubahan Kedua atas Peraturan Daerah Kabupaten Sleman Nomor 2 Tahun 2008 tentang Perusahaan Daerah Bank Perkreditan Rakyat Bank Sleman",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.03.02.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 21 Tahun 2013 tentang Penyertaan Modal Pemerintah Daerah Pada Perseroan Terbatas Bank Pembangunan Daerah Daerah Istimewa Yogyakarta",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.03.02.03",
          dasar_hukum:
            "Peraturan Daerah Nomor 17 Tahun 2016 tentang Perubahan Atas Peraturan Daerah Nomor 10 Tahun 2010 Tentang Perusahaan Daerah Air Minum Sleman",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.04.03.02",
          dasar_hukum:
            "Perjanjian Kerja Sama antara Balai Pelestarian Peninggalan Purbakala DIY, Pemerintah Kabupaten Sleman dengan PT Taman Wisata Candi Borobudur, Prambanan dan Ratu Boko tentang Pengelolaan Taman Wisata Kraton Ratu Boko Nomor 2130.M4/BP3/KKP/2010, Nomor 43/PK.KDH/A/2010 dan Nomor 282/OP.002/VIII/2010",
          skpd: ["3.26.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.04.05.01",
          dasar_hukum:
            "Keputusan Bupati Sleman Nomor 8/Kep.KDH/A/2008 tentang Penunjukan Bank Pembangunan Daerah Propinsi DIY cab. Sleman Sebagai Rekening Kas Umum Daerah Untuk Pelaksanaan APBD Kab. Sleman",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.04.07.01",
          dasar_hukum:
            "Keputusan Bupati Sleman Nomor 12/Kep.KDH/A/2012 tentang Penunjukan Bank Sebagai Penyimpan Uang Daerah",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.04.16.01",
          dasar_hukum:
            "Peratuaran Bupati Nomor 80 Tahun 2011 sebagai mana diubah dengan Peraturan Bupati Nomor 15 Tahun 2012 tentang Tarif Pelayanan Kesehatan Pada Rumah Sakit Daerah",
          skpd: ["1.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.1.04.19.01",
          dasar_hukum:
            "Peraturan Daerah Nomor 1 Tahun 2013 tentang Perubahan atas Peraturan Daerah Nomor 13 Tahun 2009 tentang Dana Penguatan Modal",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.2.01.01.01",
          dasar_hukum:
            "Surat DJPK Nomor S-170/PK/2021 tentang Penyampaian Rincian Alokasi Transfer ke Daerah dan Dana Desa Tahun Anggaran 2022",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.2.01.01.02",
          dasar_hukum:
            "Surat DJPK Nomor S-170/PK/2021 tentang Penyampaian Rincian Alokasi Transfer ke Daerah dan Dana Desa Tahun Anggaran 2022",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.2.01.01.03",
          dasar_hukum:
            "Surat DJPK Nomor S-170/PK/2021 tentang Penyampaian Rincian Alokasi Transfer ke Daerah dan Dana Desa Tahun Anggaran 2022",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.2.01.01.04",
          dasar_hukum:
            "Surat DJPK Nomor S-170/PK/2021 tentang Penyampaian Rincian Alokasi Transfer ke Daerah dan Dana Desa Tahun Anggaran 2022",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.2.01.02.01",
          dasar_hukum:
            "Surat DJPK Nomor S-170/PK/2021 tentang Penyampaian Rincian Alokasi Transfer ke Daerah dan Dana Desa Tahun Anggaran 2022",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.2.01.05.01",
          dasar_hukum:
            "Surat DJPK Nomor S-170/PK/2021 tentang Penyampaian Rincian Alokasi Transfer ke Daerah dan Dana Desa Tahun Anggaran 2022",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.2.02.01.01",
          dasar_hukum:
            "Surat DJPK Nomor S-170/PK/2021 tentang Penyampaian Rincian Alokasi Transfer ke Daerah dan Dana Desa Tahun Anggaran 2022",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.2.02.02.02",
          dasar_hukum:
            "Surat DJPK Nomor S-170/PK/2021 tentang Penyampaian Rincian Alokasi Transfer ke Daerah dan Dana Desa Tahun Anggaran 2022",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
        {
          akun: "4.3.01.01.01",
          dasar_hukum:
            "Surat DJPK Nomor S-170/PK/2021 tentang Penyampaian Rincian Alokasi Transfer ke Daerah dan Dana Desa Tahun Anggaran 2022",
          skpd: ["5.02.0.00.0.00.01.0000"],
        },
      ],
    },
  },
};
