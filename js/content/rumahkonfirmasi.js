if (config.rumah_konfirmasi) {
  $(document).on("keydown", function (e) {
    if (e.which === 65 && (e.ctrlKey || e.metaKey)) {
      setTimeout(() => {
        document.execCommand("copy");
        $("#btnOkNtpn").click();
        $("#txtkeyword").val("");
        $("#txtkeyword").focus();
        $("#txtcaptcha2").val("");
      }, 100);
    }
  });
}
