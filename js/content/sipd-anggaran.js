window.addEventListener(
  "contextmenu",
  function (event) {
    event.stopImmediatePropagation();
  },
  true
);

var title = $("title").text();
var header = $("td.atas.kanan.bawah.kiri.text_blok.text_tengah:eq(0)")
  .text()
  .replace(" PERUBAHAN", "");
var perda_lampiran_3 = {
  title: "Sistem Informasi Pemerintahan Daerah - Lampiran 3 APBD",
  header:
    "RINCIAN APBD MENURUT URUSAN PEMERINTAHAN DAERAH, ORGANISASI, PENDAPATAN, BELANJA DAN PEMBIAYAAN",
};
var penjabaran_lampiran_2 = {
  title: "Sistem Informasi Pemerintahan Daerah - Lampiran 2 APBD",
  header:
    "PENJABARAN APBD MENURUT URUSAN PEMERINTAHAN DAERAH, ORGANISASI, PROGRAM, KEGIATAN, SUB KEGIATAN, KELOMPOK, JENIS, OBJEK, RINCIAN OBJEK, SUB RINCIAN OBJEK PENDAPATAN, BELANJA, DAN PEMBIAYAAN",
};

var is_perda_lampiran_3 =
  title == perda_lampiran_3.title && header.includes(perda_lampiran_3.header);
var is_penjabaran_lampiran_2 =
  title == penjabaran_lampiran_2.title &&
  header.includes(penjabaran_lampiran_2.header);

// set title
if (is_perda_lampiran_3 || is_penjabaran_lampiran_2) {
  var laporan = "Lampiran 3 APBD - ";
  if (is_penjabaran_lampiran_2) {
    laporan = "Lampiran 2 APBD - ";
  }
  var skpd = $('td:contains("Organisasi")').eq(2).next().next().text();
  var nm_skpd = skpd.substring(17);
  var kd_skpd = $('td:contains("BELANJA")')
    .eq(2)
    .prev()
    .prev()
    .prev()
    .prev()
    .prev()
    .prev()
    .prev();
  if (is_penjabaran_lampiran_2) {
    kd_skpd = kd_skpd.prev().prev();
  }
  kd_skpd = kd_skpd.text();
  setTitle(laporan + kd_skpd + nm_skpd);
  $("script").remove();
}

// dasar hukum perda
if (config.sipd.dasar_hukum.perda && is_perda_lampiran_3) {
  var is_apbd_perubahan = $('td:contains("Sesudah Perubahan")').length != 0;
  var kolom_dasar_hukum = is_apbd_perubahan ? 13 : 11;
  // dasar hukum gaji
  var el = $('td:contains("Penyediaan Gaji dan Tunjangan ASN")').next().next();
  if (is_apbd_perubahan) {
    el = el.next().next();
  }
  el.css("fontSize", "smaller");
  el.text(config.sipd.dasar_hukum.gaji);
  var skpd = $('td:contains("Organisasi")').eq(2).next().next().text();
  var kd_skpd = skpd.substring(0, 17) + ".0000";
  // dasar hukum pendapatan
  var rek = $("table tbody")
    .eq(3)
    .children()
    .each(function () {
      var self = this;
      var arr_akun = [];
      for (var i = 6; i < 9; i++) {
        if ($(self).children().eq(i).text()) {
          arr_akun.push($(this).children().eq(i).text());
        }
      }
      var dasar_hukum_akun = [];
      var akun = arr_akun.join(".");
      if (arr_akun[0] && arr_akun[0] == "4") {
        config.sipd.dasar_hukum.pendapatan.forEach(function (data) {
          if (
            data.akun.substring(0, 6) == akun &&
            $.inArray(kd_skpd, data.skpd) !== -1
          ) {
            dasar_hukum_akun.push(data.dasar_hukum);
          }
        });
      }
      if (dasar_hukum_akun.length) {
        dasar_hukum_akun = dasar_hukum_akun.filter(function (x, i, a) {
          return a.indexOf(x) == i;
        });
        var prefix = dasar_hukum_akun.length > 1 ? "- " : "";
        dasar_hukum_akun.forEach(function (dasar_hukum) {
          $(self)
            .children()
            .eq(kolom_dasar_hukum)
            .append(prefix + dasar_hukum + "<br/>")
            .css("fontSize", "smaller");
        });
      }
    });
}
// dasar hukum penjabaran
if (config.sipd.dasar_hukum.penjabaran && is_penjabaran_lampiran_2) {
  var is_apbd_perubahan = $('td:contains("Sesudah Perubahan")').length != 0;
  var kolom_dasar_hukum = is_apbd_perubahan ? 16 : 14;
  // dasar hukum gaji
  var el = $('td:contains("Penyediaan Gaji dan Tunjangan ASN")')
    .next()
    .next()
    .next();
  if (is_apbd_perubahan) {
    el = el.next().next();
  }
  el.css("fontSize", "smaller");
  el.text(config.sipd.dasar_hukum.gaji);
  // dasar hukum pendapatan
  var rek = $("table tbody")
    .eq(3)
    .children()
    .each(function () {
      var self = this;
      var arr_akun = [];
      for (var i = 6; i < 11; i++) {
        if ($(self).children().eq(i).text()) {
          arr_akun.push($(this).children().eq(i).text());
        }
      }

      var dasar_hukum_akun = [];
      var akun = arr_akun.join(".");
      if (arr_akun[0] && arr_akun[0] == "4") {
        config.sipd.dasar_hukum.pendapatan.forEach(function (data) {
          if (data.akun == akun) {
            dasar_hukum_akun.push(data.dasar_hukum);
          }
        });
      }
      if (dasar_hukum_akun.length) {
        dasar_hukum_akun = dasar_hukum_akun.filter(function (x, i, a) {
          return a.indexOf(x) == i;
        });
        var prefix = dasar_hukum_akun.length > 1 ? "- " : "";
        dasar_hukum_akun.forEach(function (dasar_hukum) {
          $(self)
            .children()
            .eq(kolom_dasar_hukum)
            .append(prefix + dasar_hukum + "<br/>")
            .css("fontSize", "smaller");
        });
      }
    });
}

// hilangkan ttd perda & penjabaran
if (is_perda_lampiran_3 || is_penjabaran_lampiran_2) {
  var skpd = $('td:contains("Organisasi")').eq(2).next().next().text();
  skpd = skpd.substr(0, 17);
  if (skpd != "8.01.1.05.0.00.01") {
    $('td:contains("KUSTINI SRI PURNOMO")').text("");
  }
}

function setTitle(title) {
  $("title").text(title);
}

// Perda APBD
// if (title == "Sistem Informasi Pemerintahan Daerah - Lampiran 1 APBD") {
//   setTitle("Perda APBD Lampiran I");
//   var el = $('*:contains("Lampiran I")');
//   setPerda(el);
// }
// Penjabaran APBD
// function setPerda(el) {
//   // Nomor Perda
//   el.parent().next().children().last().text(config.sipd.perda.nomor);
//   // Tanggal Perda
//   el.parent().next().next().children().last().text(config.sipd.perda.tanggal);
// }

// Bagi 2 nilai anggaran
if (config.sipd.penjabaran.divide_nilai && is_penjabaran_lampiran_2) {
  var el = $('*:contains("Jumlah Pendapatan")');
  el.parent()
    .siblings()
    .each(function (index, value) {
      if (index < 2) {
        return true;
      }
      var rekening = [
        $(this).find("td:eq(6)").text(),
        $(this).find("td:eq(7)").text(),
        $(this).find("td:eq(8)").text(),
        $(this).find("td:eq(9)").text(),
        $(this).find("td:eq(10)").text(),
      ];
      var kd_rekening = rekening
        .filter(function (val) {
          return val;
        })
        .join(".");
      // skip
      // PENDAPATAN DAERAH
      // BELANJA
      // PEMBIAYAAN
      if (["4", "5", "6"].includes(kd_rekening)) {
        return true;
      }
      var divide_by = 1;
      var index = 12;
      // PENDAPATAN
      if (rekening[0] == "4") {
        divide_by = rekening[3] ? 2 : 4;
      }
      // BELANJA
      // PROGRAM, KEGIATAN, SUBKEGIATAN
      if ($(this).find("td:eq(5)").text() && !$(this).find("td:eq(6)").text()) {
        divide_by = 2;
      }
      // REKENING BELANJA
      if (rekening[0] == "5") {
        divide_by = 1;
      }
      // PEMBIAYAAN
      if (rekening[0] == "6") {
        divide_by = rekening[3] ? 2 : 4;
      }
      if (
        [
          // "Jumlah Pendapatan",
          "Jumlah Penerimaan Pembiayaan",
          "Jumlah Penerimaan Pengeluaran",
        ].includes($(this).find("td:eq(0)").text())
      ) {
        divide_by = 4;
        index = 1;
      }
      if (
        ["Jumlah Belanja", "Total Surplus/(Defisit)"].includes(
          $(this).find("td:eq(0)").text()
        )
      ) {
        divide_by = 2;
        index = 1;
      }
      var nilai = $(this)
        .find("td:eq(" + index + ")")
        .text()
        .replace(/\./g, "")
        .trim();
      var negatif = false;
      if (nilai[0] == "(") {
        nilai = nilai.replace(/\(/g, "");
        nilai = nilai.replace(/\)/g, "");
        negatif = true;
      }
      var nilai = formatRupiah((nilai / divide_by).toString());
      if (negatif) {
        nilai = "(" + nilai + ")";
      }
      $(this)
        .find("td:eq(" + index + ")")
        .text(nilai);
      $(this).css("background-color", "#ffcccc");
    });
}
