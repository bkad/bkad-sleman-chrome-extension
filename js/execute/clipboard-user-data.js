function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
}

function getTokenSIPD() {
  return getCookie("X-SIPD-PU-TK");
}

function getUserProfileSIPD() {
  var token_sipd = getTokenSIPD();
  var url = "https://service.sipd.kemendagri.go.id/auth/strict/user/profile";
  var headers = {
    authorization: "Bearer " + token_sipd,
    "Content-Type": "application/json",
  };
  return axios.get(url, { headers: headers });
}

function sendMessage(text, success) {
  chrome.runtime.sendMessage({
    success: success,
    text: text,
  });
}

getUserProfileSIPD()
  .then(function (response) {
    var user = response.data;
    var token = getTokenSIPD();
    var text = JSON.stringify({
      ...user,
      token: token,
    });
    navigator.clipboard.writeText(text).then(
      function () {
        sendMessage("Copying to clipboard was successful!", true);
      },
      function (err) {
        sendMessage(err, false);
        console.error("Could not copy text: ", err);
      }
    );
  })
  .catch(function (err) {
    sendMessage(err, false);
    console.log(err);
  });
