function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
}

function getTokenSIPD() {
  return getCookie("X-SIPD-PU-TK");
}

function sendMessage(text, success) {
  chrome.runtime.sendMessage({
    success: success,
    text: text,
  });
}

async function getUserProfileSIPD() {
  try {
    var nama_role = await getNamaRole();
    var token_sipd = getTokenSIPD();
    var url = "https://service.sipd.kemendagri.go.id/auth/strict/user/profile";
    var headers = {
      authorization: "Bearer " + token_sipd,
      "Content-Type": "application/json",
    };
    var response = await axios.get(url, { headers: headers });
    return { ...response.data, nama_role: nama_role };
  } catch (error) {
    var message = error.response.data.msg
      ? error.response.data.msg
      : error.message;
    sendMessage(message, false);
    console.log(error);
  }
}

async function getNamaRole() {
  try {
    var $nama_role = document.querySelector(
      ".container-profile-fixed-description"
    );
    if ($nama_role) {
      return $nama_role.lastChild.innerHTML;
    }
    var $nav = document.querySelector(".css-jr-2-kfawf");
    if (!$nav) {
      return "";
    }
    $nav.click();
    await sleep(1);
    $nama_role = document.querySelector(".container-profile-fixed-description");
    var nama_role = $nama_role ? $nama_role.lastChild.innerHTML : null;
    $nav.click();
    return nama_role;
  } catch (error) {
    sendMessage(error.message, false);
    console.log(error);
  }
}

async function updateUser(user) {
  try {
    var token_sipd = getTokenSIPD();
    var api_key = "VuuY3AhTun30Q99bWWDcclDAsrAyISAKBGJjcSr6";
    var url = "http://localhost:8888/sipd-clicker/index.php/api/user_sipd";
    var data = {
      token: token_sipd,
      nip_user: user.nip_user,
      nama_user: user.nama_user,
      nama_role: user.nama_role,
    };
    var headers = {
      "X-API-KEY": api_key,
    };
    var response = await axios.put(url, data, { headers: headers });
    return response.data;
  } catch (error) {
    var message = error.message;
    if (error.response.data.error) {
      message = error.response.data.error;
    } else if (error.response.data.message) {
      message = error.response.data.message;
    }
    sendMessage(message, false);
    console.log(error);
  }
}

async function main() {
  var userData = await getUserProfileSIPD();
  if (!userData) {
    return;
  }
  var response = await updateUser(userData);
  if (response) {
    sendMessage(
      `User Data ${userData.nama_role} ${userData.nama_user} berhasil disinkronisasi`,
      true
    );
  }
}

main();
