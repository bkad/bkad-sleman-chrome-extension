function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
}

function getTokenSIPD() {
  return getCookie("X-SIPD-PU-TK");
}

function sendMessage(text, success) {
  chrome.runtime.sendMessage({
    success: success,
    text: text,
  });
}

var text = getTokenSIPD();
navigator.clipboard.writeText(text).then(
  function () {
    sendMessage("Copying to clipboard was successful!", true);
  },
  function (err) {
    sendMessage(err, false);
    console.error("Could not copy text: ", err);
  }
);
