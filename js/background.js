const MAIN_MENU = "MAIN_MENU";
const SYNC_USER_DATA_MENU = "SYNC_USER_DATA_MENU";
const COPY_TOKEN_MENU = "COPY_TOKEN_MENU";
const COPY_USER_DATA_MENU = "COPY_USER_DATA_MENU";

chrome.runtime.onInstalled.addListener(() => {
  chrome.contextMenus.create({
    id: MAIN_MENU,
    title: "SIPD Clicker",
    contexts: ["all"],
    documentUrlPatterns: ["https://sipd.kemendagri.go.id/penatausahaan/*"],
  });
  chrome.contextMenus.create({
    id: SYNC_USER_DATA_MENU,
    title: "Sync User Data",
    parentId: MAIN_MENU,
    contexts: ["all"],
  });
  // chrome.contextMenus.create({
  //   id: COPY_USER_DATA_MENU,
  //   title: "Copy User Data",
  //   parentId: MAIN_MENU,
  //   contexts: ["all"],
  // });
  // chrome.contextMenus.create({
  //   id: COPY_TOKEN_MENU,
  //   title: "Copy Token",
  //   parentId: MAIN_MENU,
  //   contexts: ["all"],
  // });
});

chrome.contextMenus.onClicked.addListener((info, tab) => {
  if (info.menuItemId === SYNC_USER_DATA_MENU) {
    executeScript(["js/axios.min.js", "js/execute/sync-user-data.js"]);
  } else if (info.menuItemId === COPY_USER_DATA_MENU) {
    executeScript(["js/axios.min.js", "js/execute/clipboard-user-data.js"]);
  } else if (info.menuItemId === COPY_TOKEN_MENU) {
    executeScript(["js/execute/clipboard-token.js"]);
  }
});

async function executeScript(files) {
  const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
  await chrome.scripting.executeScript({
    target: { tabId: tab.id },
    files: files,
  });
}

function notify(message, status) {
  chrome.notifications.create({
    type: "basic",
    iconUrl: status ? "/img/logo-sleman.png" : "/img/error.png",
    title: "SIPD Clicker",
    message: message,
  });
}

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
  notify(msg.text, msg.success);
});
