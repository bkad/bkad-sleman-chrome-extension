var el = document.querySelector(".dataTables_scrollBody");
if (el.style.height == "" && el.style.maxHeight == "50vh") {
  el.style.maxHeight = "";
  el.style.height = "50vh";
}
el.style.height = el.style.height == "50vh" ? "" : "50vh";
